extern crate proc_macro;

use syn::parse_macro_input;

use proc_macro::{TokenStream, TokenTree};
use proc_macro_hack::proc_macro_hack;
use quote::quote;
use syn::parse::{Parse, ParseStream};

#[proc_macro_hack]
pub fn between(input: TokenStream) -> TokenStream {
    let idents = input
        .into_iter()
        .map(|i| match i {
            TokenTree::Ident(b) => {
                b
            },
            _ => panic!()
        })
        .collect::<Vec<_>>();

    let new_token_stream = quote! {
        (#(#idents),*)
    };

    new_token_stream.into()
}