use proc_macro_hack::proc_macro_hack;

#[proc_macro_hack]
pub use pm::between;

fn main() {
    let a = 1;
    let b = 2;

    // Expand to (a, b);
    between!(a, b);
}